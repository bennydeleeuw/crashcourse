package nl.wehkamp.crashcourse.login;

import androidx.annotation.Nullable;

public class LoginResult {

    private boolean isLoggedIn;
    private String message;

    public LoginResult(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public LoginResult(boolean isLoggedIn, String message) {
        this.isLoggedIn = isLoggedIn;
        this.message = message;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public boolean equals(@Nullable Object obj) {

        try {
            return ((LoginResult) obj).isLoggedIn == this.isLoggedIn &&
                    ((LoginResult) obj).getMessage().equals(this.message);

        } catch (Exception ex) {
            return false;
        }
    }
}
