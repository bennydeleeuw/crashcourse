package nl.wehkamp.crashcourse.login;

import io.reactivex.Observable;
import io.reactivex.Single;

public class LoginService {

    public static String INCORRECT_PASSWORD = "Incorrect Password";

    public Observable<Boolean> login(String password) {

        return Observable.just(password)
                .doOnNext(p -> {
                    if (!p.equals("test1234")) throw new RuntimeException(INCORRECT_PASSWORD);
                })
                .map(r -> true);
    }
}
