package nl.wehkamp.crashcourse.auto;

import org.junit.Test;

import nl.wehkamp.crashcourse.vehicle.AutoDi;
import nl.wehkamp.crashcourse.vehicle.BrandstofService;

import static org.junit.Assert.assertEquals;

public class Auto_Di_Test {


    @Test
    public void test_auto_di_prijs_volle_tank() {

        BrandstofService brandstofService = new BrandstofService() {
            @Override
            public float getPrijsPerLiter() {
                return 1.47f;
            }
        };

        AutoDi auto = new AutoDi(50, brandstofService);
        float result = auto.geefTotalePrijs();
        assertEquals(73.5, result, 0);

    }

}
