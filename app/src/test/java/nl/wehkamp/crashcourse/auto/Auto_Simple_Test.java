package nl.wehkamp.crashcourse.auto;

import org.junit.Test;

import nl.wehkamp.crashcourse.vehicle.AutoSimple;

import static org.junit.Assert.*;


public class Auto_Simple_Test {

    @Test
    public void test_auto_simple_prijs_volle_tank() {
        AutoSimple auto = new AutoSimple();
        float result = auto.geefTotalePrijs();
        assertEquals(71.5, result, 0);
    }

}
