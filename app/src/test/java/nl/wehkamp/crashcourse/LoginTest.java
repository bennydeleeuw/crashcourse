package nl.wehkamp.crashcourse;

import org.junit.Test;

import io.reactivex.Observable;
import nl.wehkamp.crashcourse.login.LoginResult;
import nl.wehkamp.crashcourse.login.LoginService;

public class LoginTest {

    LoginService loginService = new LoginService();

    //Fix this function so  all the tests will succeed
    private Observable<LoginResult> login(String password) {
        return loginService.login(password)
                .map(LoginResult::new);
    }


    @Test
    public void login_success() {

        login("test1234")
                .map(LoginResult::isLoggedIn)
                .test()
                .assertResult(true)
                .assertComplete();

    }


    @Test
    public void login_failed() {

        login("xxxx")
                .map(LoginResult::isLoggedIn)
                .test()
                .assertResult(false)
                .assertComplete();


    }

    @Test
    public void login_failed_message() {

        login("xxxx")
                .map(LoginResult::getMessage)
                .test()
                .assertResult(LoginService.INCORRECT_PASSWORD)
                .assertComplete();
    }
}
