package nl.wehkamp.crashcourse;

import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.subjects.*;
import io.reactivex.subjects.Subject;

public class SubjectsTest {

    //Fix this function so it returns the right subject.
    private Subject<Integer> createSubject() {
        return PublishSubject.create();
    }

    @Test
    public void test() {

        Subject<Integer> testSubject = createSubject();

        Observable.just(1,2,3)
                .subscribe(testSubject);


        testSubject.test().assertResult(1, 2, 3).assertComplete();
        testSubject.test().assertResult(1, 2, 3).assertComplete();
    }



}
