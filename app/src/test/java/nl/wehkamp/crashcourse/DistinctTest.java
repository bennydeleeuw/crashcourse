package nl.wehkamp.crashcourse;

import org.junit.Test;

import io.reactivex.Observable;

public class DistinctTest {

    Observable<Integer> obs = Observable.fromArray(1, 1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4);
    Observable<Integer> obs2 = Observable.fromArray(2, 2, 2, 3, 3, 4, 2, 1, 2, 3, 4, 1, 2, 3, 4);

    @Test
    public void distinct_test_1() {
        //Fix this test
        obs.test()
                .assertResult(1, 2, 3, 4);

    }


    @Test
    public void distinct_test_2() {
        //Fix this test
        obs2.test()
                .assertResult(1, 2, 3, 4);

    }

}
