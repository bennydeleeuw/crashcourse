package nl.wehkamp.crashcourse;

import org.junit.Test;

import io.reactivex.Observable;

public class EmptyTest {

    Observable<Integer> a(int filter) {
        return Observable.just(1, 3, 5, 7, 9).filter(i -> i % filter == 0);
    }

    Observable<Integer> b(int filter) {
        return Observable.just(0, 2, 4, 6, 8).filter(i -> i % filter == 0);
    }

    //Make both tests work by modding this method
    public Observable<Integer> combine(int filter) {
        return a(filter);
    }

    @Test
    public void combine_two_observers_1() {

        combine(1)
                .test()
                .assertResult(1, 3, 5, 7, 9);
    }

    @Test
    public void combine_two_observers_2() {

        combine(2)
                .test()
                .assertResult(0, 2, 4, 6, 8);
    }


}
