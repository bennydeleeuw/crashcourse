package nl.wehkamp.crashcourse;

import org.junit.Test;

import io.reactivex.Single;


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ConvertToObservable {

    private interface NameListener {
        void nameIsReady(String fullname);
    }

    public void getConcatName(String firstname, String lastname, NameListener listener) {
        listener.nameIsReady(String.format("%s %s", firstname, lastname));
    }

    public Single<String> getConcatNameObservable() {
        //Implement this function so it wraps the listener in a Single
        return Single.just("Implement this...");
    }


    @Test
    public void convertToObservable() {
        getConcatNameObservable().test().assertResult("Voornaam Achternaam").assertComplete();
    }


}