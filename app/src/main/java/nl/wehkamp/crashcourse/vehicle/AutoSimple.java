package nl.wehkamp.crashcourse.vehicle;

public class AutoSimple  {

    private int tankInhoud = 50;
    private float prijsPerLiter = 1.43f;

    public float geefTotalePrijs() {
        return tankInhoud * prijsPerLiter;
    }

}
