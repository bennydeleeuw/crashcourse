package nl.wehkamp.crashcourse.vehicle;

public class Tank {

    private final int inhoud;


    public Tank(int inhoud) {
        this.inhoud = inhoud;
    }

    public int getInhoud() {
        return inhoud;
    }
}
