package nl.wehkamp.crashcourse.vehicle;

public class AutoDi {

    private final int tankInhoud;
    private final BrandstofService brandstofService;

    public AutoDi(int tankInhoud, BrandstofService brandstofService) {
        this.tankInhoud = tankInhoud;
        this.brandstofService = brandstofService;
    }

    public float geefTotalePrijs() {
        return tankInhoud * brandstofService.getPrijsPerLiter();
    }

}
