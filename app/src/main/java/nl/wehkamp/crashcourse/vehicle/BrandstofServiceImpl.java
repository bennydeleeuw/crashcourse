package nl.wehkamp.crashcourse.vehicle;

public class BrandstofServiceImpl implements BrandstofService {

    @Override
    public float getPrijsPerLiter() {
        return 1.47f;
    }
}
