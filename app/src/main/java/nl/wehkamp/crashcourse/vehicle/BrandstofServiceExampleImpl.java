//package nl.wehkamp.crashcourse.vehicle;
//
//public class BrandstofServiceExampleImpl implements BrandstofService {
//
//    private final UrlProvider urlProvider;
//    private final HeaderProvider headerProvider;
//    private final Executor executor;
//
//    public BrandstofServiceExampleImpl(UrlProvider urlProvider, HeaderProvider headerProvider, Executor executor) {
//        this.urlProvider = urlProvider;
//        this.headerProvider = headerProvider;
//        this.executor = executor;
//    }
//
//    @Override
//    public float getPrijsPerLiter() {
//        return executor.withUrl(urlProvider.getUrl()).withHeaders(headerProvider.getHeaders()).get();
//    }
//}
